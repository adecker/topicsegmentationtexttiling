import readFriends
from sklearn.metrics import f1_score, precision_score, recall_score
import itertools
import utils, generate_pdf_results
from nltk.metrics import segmentation as seg_mod
import pandas as pd
import numpy as np
import random as rd
import argparse

def rd_baseline(xp_name, seasons):
    dataset = readFriends.FriendsDataset()

    all_ep = []
    all_f1 = []
    all_fk1 = []
    all_fk2 = []
    all_pk = []

    for season in seasons:

        num_ep = dataset.dataset_stats['Episodes'].loc[str(season)]
        for ep in range(1, num_ep + 1):
            all_ep.append(f's{season}e{ep}')

            df = dataset.as_df(dataset.get_episode(season, ep))
            segmentation_gold = []
            for _, l in itertools.groupby(df['topics_scenes_and_notes']):
                segmentation_gold += [0]*(sum(1 for _ in l)-1) + [1]
            segmentation_gold = segmentation_gold[:-1]
            
            # build random topic segmentation
            p = len(segmentation_gold)
            nb_ts = rd.randint(0, p)
            segmentation_hyp = list(np.random.binomial(1, nb_ts/p, p))

            # evaluate against gold segmentation
            # pr = precision_score(segmentation_gold, segmentation_hyp)
            # rec = recall_score(segmentation_gold, segmentation_hyp)
            f1 = f1_score(segmentation_gold, segmentation_hyp)
            prk2, reck2, fk2 = utils.Fk(segmentation_gold, segmentation_hyp, k=2)
            prk1, reck1, fk1 = utils.Fk(segmentation_gold, segmentation_hyp, k=1)
            pk = seg_mod.pk(segmentation_gold, segmentation_hyp, boundary=1)
            
            # save results
            all_f1.append(f1)
            all_fk1.append(fk1)
            all_fk2.append(fk2)
            all_pk.append(pk)

    df = pd.DataFrame([all_f1, all_fk1, all_fk2, all_pk], index=['F1', 'Fk1', 'Fk2', 'Pk'], columns=all_ep)
    df.to_csv(f"../results/{xp_name}.csv")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to create a random topic segmentation of some episodes of Friends.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-x", "--experience-name", type=str, default='RdBaseline', help="name of the experiment")
    parser.add_argument("-s", "--season", nargs='+', type=int, default=[1,2,3,4,5,6,7,8,9,10], help="season(s) we train on")
    parser.add_argument("-p", "--generate-pdf", type=bool, default=False, help="to generate a pdf of the results")
    args = parser.parse_args()
    config = vars(args)

    xp_name = config["experience_name"]
    seasons = config["season"]

    rd_baseline(xp_name, seasons)
    
    if config["generate_pdf"]:
        generate_pdf_results.generate_pdf(xp_name, seasons = seasons)