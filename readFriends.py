import pandas as pd
import os
import fnmatch

class FriendsDataset:
    def __init__(self):
        dataset_stats = pd.read_csv("./stats_dataset.csv", sep=';', header=0, index_col=0)
        self.__dict__.update(locals())
        del self.__dict__["self"]

    def get_season(self, s):
        if s > 10 or s < 1:
            raise ValueError("The season must be between 1 and 10.")
        all_files = fnmatch.filter(os.listdir("./FriendsSceneSeg/"), f's{"0" + str(s) if s < 10 else str(s)}_*')
        all_files.sort()
        return all_files

    def get_episode(self, s, e):
        if s > 10 or s < 1:
            raise ValueError("The season must be between 1 and 10.")
        num_episodes = self.dataset_stats.loc[str(s), 'Episodes']
        if e < 1 or e > num_episodes:
            raise ValueError(f"The episode must be between 1 and {num_episodes} for season {s}.")
        all_files = fnmatch.filter(os.listdir("./FriendsSceneSeg/"), f's{"0" + str(s) if s < 10 else str(s)}_e{"0" + str(e) if e < 10 else str(e)}_*')
        all_files.sort()
        return all_files
    
    def as_df(self, files_list, discard_one_utt_scenes = False, speakers = False):
        df_list = [pd.read_csv("./FriendsSceneSeg/" + scene, sep='\t', names=['speakers', 'utterance'], engine='python', quotechar='\x07') for scene in files_list]
        if discard_one_utt_scenes:
            df_list = [df for df in df_list if len(df) > 1]
        j = 0
        for i, df in enumerate(df_list):
            df['topics_scenes'] = [i]*len(df)
            df['topics_scenes_and_notes'] = df['speakers'].isna().cumsum() + [j]*len(df)
            j = df['topics_scenes_and_notes'].iloc[-1] + 1
        df = pd.concat(df_list, ignore_index=True)
        df['utterance'].fillna('(blank)', inplace=True)
        df.dropna(subset = ['speakers'], inplace=True)
        if speakers:
            df['utterance'] = df['speakers'].astype(str) + ": " + df["utterance"]
        return df
    
    def as_str(self, files_list):
        txt = ""
        for file in files_list:
            with open("./FriendsSceneSeg/" + file, 'r') as f:
                txt += f.read()
        return txt
    
    def as_content_str(self, files_list, speakers = False):
        df_list = [pd.read_csv("./FriendsSceneSeg/" + scene, sep='\t', names=['speakers', 'utterance'], engine='python', quotechar='\x07') for scene in files_list]
        df = pd.concat(df_list, ignore_index=True)
        df['utterance'].fillna('(blank)', inplace=True)
        df.dropna(subset = ['speakers'], inplace=True)
        if speakers:
            df['utterance'] = df['speakers'].astype(str) + ": " + df["utterance"]
        return '\n'.join(df['utterance'])

    def as_segmented_content_str(self, files_list, speakers = False):
        df_list = [pd.read_csv("./FriendsSceneSeg/" + scene, sep='\t', names=['speakers', 'utterance'], engine='python', quotechar='\x07') for scene in files_list]
        j = 0
        for i, df in enumerate(df_list):
            df['topics_scenes'] = [i]*len(df)
            df['topics_scenes_and_notes'] = df['speakers'].isna().cumsum() + [j]*len(df)
            j = df['topics_scenes_and_notes'].iloc[-1] + 1
        df = pd.concat(df_list, ignore_index=True)
        df['utterance'].fillna('(blank)', inplace=True)
        df.dropna(subset = ['speakers'], inplace=True)
        if speakers:
            df['utterance'] = df['speakers'].astype(str) + ": " + df["utterance"]
        return '\n\n<TS>\n\n'.join(df.groupby(['topics_scenes_and_notes']).agg({'utterance': lambda x: '\n'.join(x)})['utterance']) #'speakers': lambda x: '\n'.join(x), 
    
    def stats_topics(self):
        import itertools
        import numpy as np
        max_num_ep = max(self.dataset_stats['Episodes'][:-1])
        results = []
        for season in range(1,11):
            num_ep = self.dataset_stats['Episodes'].loc[str(season)]
            season_results = []
            for i in range(1, num_ep+1):
                df = self.as_df(self.get_episode(season, i))
                segmentation_gold = []
                for _, l in itertools.groupby(df['topics_scenes_and_notes']):
                    segmentation_gold += [0]*(sum(1 for _ in l)-1) + [1]
                season_results.append(sum(segmentation_gold) - 1)
            if len(season_results) < max_num_ep:
                season_results += [np.nan]*(max_num_ep - len(season_results))
            season_results.append(np.nanmean(season_results))
            results.append(season_results)
        df_topics = pd.DataFrame(results, index=list(range(1,11)), columns = list(range(1, max_num_ep+1)) + ['Mean'])
        return df_topics
    
if __name__ == '__main__':
    dataset = FriendsDataset()
    dataset.stats_topics()
    print(dataset.as_segmented_content_str(dataset.get_episode(1, 2)))