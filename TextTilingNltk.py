# Natural Language Toolkit: TextTiling
#
# Copyright (C) 2001-2023 NLTK Project
# Author: George Boutsioukis
#
# URL: <https://www.nltk.org/>
# For license information, see LICENSE.TXT
# Modifications by: Authors of the paper


import math
import re
import pandas as pd
from nltk.metrics import segmentation as seg_mod
import numpy
import utils
from nltk.tokenize.api import TokenizerI
from sklearn.metrics import f1_score, precision_score, recall_score
import readFriends
import itertools
import argparse

BLOCK_COMPARISON, VOCABULARY_INTRODUCTION = 0, 1
LC, HC = 0, 1
DEFAULT_SMOOTHING = [0]
FLAT_WINDOW, HANNING_WINDOW, HAMMING_WINDOW, BARTLETT_WINDOW, BLACKMAN_WINDOW = "flat", "hanning", "hamming", "bartlett", "blackman"


class TextTilingTokenizer(TokenizerI):
    """Tokenize a document into topical sections using the TextTiling algorithm.
    This algorithm detects subtopic shifts based on the analysis of lexical
    co-occurrence patterns.

    The process starts by tokenizing the text into pseudosentences of
    a fixed size w. Then, depending on the method used, similarity
    scores are assigned at sentence gaps. The algorithm proceeds by
    detecting the peak differences between these scores and marking
    them as boundaries. The boundaries are normalized to the closest
    paragraph break and the segmented text as well as the list of 
    boundaries are returned.

    :param w: Pseudosentence size
    :type w: int
    :param k: Size (in sentences) of the block used in the block comparison method
    :type k: int
    :param memory_size: Size (in sentences) of the memory used in the vocabulary introduction method
    :type memory_size: int
    :param similarity_method: The method used for determining similarity scores:
       `BLOCK_COMPARISON` (default) or `VOCABULARY_INTRODUCTION`.
    :type similarity_method: constant
    :param stopwords: A list of stopwords that are filtered out (defaults to NLTK's stopwords corpus)
    :type stopwords: list(str)
    :param smoothing_method: The method used for smoothing the score plot:
      `DEFAULT_SMOOTHING` (default)
    :type smoothing_method: constant
    :param smoothing_window: The type of window used for smoothing the score plot:
      `FLAT_WINDOW` (default), HANNING_WINDOW, HAMMING_WINDOW, BARTLETT_WINDOW or BLACKMAN_WINDOW
    :type smoothing_window: constant
    :param smoothing_width: The width of the window used by the smoothing method
    :type smoothing_width: int
    :param smoothing_rounds: The number of smoothing passes
    :type smoothing_rounds: int
    :param cutoff_policy: The policy used to determine the number of boundaries:
      `HC` (default) or `LC`
    :type cutoff_policy: constant
    """

    def __init__(
        self,
        w=12,
        k=10,
        memory_size=20,
        similarity_method=BLOCK_COMPARISON,
        stopwords=None,
        smoothing_method=DEFAULT_SMOOTHING,
        smoothing_window=FLAT_WINDOW,
        smoothing_width=2,
        smoothing_rounds=1,
        cutoff_policy=HC,
    ):

        if stopwords is None:
            from nltk.corpus import stopwords

            stopwords = stopwords.words("english")
        self.__dict__.update(locals())
        del self.__dict__["self"]


    def tokenize(self, text):
        """Return a tokenized copy of *text*, where each "token" represents
        a separate topic."""

        lowercase_text = text.lower()
        paragraph_breaks = self._mark_paragraph_breaks(text)
        text_length = len(lowercase_text)

        # Tokenization step starts here

        # Remove punctuation
        nopunct_text = "".join(
            c for c in lowercase_text if re.match(r"[a-z\-' \n\t]", c)
        )
        nopunct_par_breaks = self._mark_paragraph_breaks(nopunct_text)

        tokseqs = self._divide_to_tokensequences(nopunct_text)

        # Filter stopwords
        for ts in tokseqs:
            ts.wrdindex_list = [
                wi for wi in ts.wrdindex_list if wi[0] not in self.stopwords
            ]

        # The morphological stemming step mentioned in the TextTile
        # paper is not implemented.  A comment in the original C
        # implementation states that it offers no benefit to the
        # process. It might be interesting to test the existing
        # stemmers though. (s.th. like words = _stem_words(words))

        token_table = self._create_token_table(tokseqs, nopunct_par_breaks)
        # End of the Tokenization step

        # Lexical score determination
        if self.similarity_method == BLOCK_COMPARISON:
            gap_scores = self._block_comparison(tokseqs, token_table)
        elif self.similarity_method == VOCABULARY_INTRODUCTION:
            gap_scores = self._vocabulary_introduction(tokseqs, token_table, self.memory_size)
        else:
            raise ValueError(
                f"Similarity method {self.similarity_method} not recognized"
            )

        if self.smoothing_method == DEFAULT_SMOOTHING:
            smooth_scores = self._smooth_scores(gap_scores, self.smoothing_window)
        else:
            raise ValueError(f"Smoothing method {self.smoothing_method} not recognized")
        # End of Lexical score Determination

        # Boundary identification
        depth_scores = self._depth_scores(smooth_scores)
        segment_boundaries = self._identify_boundaries(depth_scores)

        normalized_boundaries = self._normalize_boundaries(
            text, segment_boundaries, paragraph_breaks
        )
        # End of Boundary Identification
        segmented_text = []
        prevb = 0 # previous boundary

        for b in normalized_boundaries:
            if b == 0: # ie. we are at the very begining
                continue
            segmented_text.append(text[prevb:b]) # create a list of pieces of text where each piece is a topic
            prevb = b

        if prevb < text_length:  # append any text that may be remaining
            segmented_text.append(text[prevb:])

        if not segmented_text: # if no segment yet, return the whole text, ie. 1 single topic
            segmented_text = [text]
        
        adjusted_boundaries = []
        for tile in segmented_text:
            num_utterances = len(tile.strip('\n').split('\n\n'))
            adjusted_boundaries += [1] + [0]*(num_utterances-1)

        return segmented_text, adjusted_boundaries[1:]


    def _block_comparison(self, tokseqs, token_table):
        """Implements the block comparison method"""

        def blk_frq(tok, block):
            ts_occs = filter(lambda o: o[0] in block, token_table[tok].ts_occurences) # keeps only the elements of token_table[tok].ts_occurences if the index is in block
            freq = sum(tsocc[1] for tsocc in ts_occs) # total number of occurences in all the token sequences
            return freq

        gap_scores = []
        numgaps = len(tokseqs) - 1

        for curr_gap in range(numgaps):
            score_dividend, score_divisor_b1, score_divisor_b2 = 0.0, 0.0, 0.0
            score = 0.0
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < self.k - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - self.k:
                window_size = numgaps - curr_gap
            else:
                window_size = self.k

            b1 = [ts.index for ts in tokseqs[curr_gap - window_size + 1 : curr_gap + 1]]
            b2 = [ts.index for ts in tokseqs[curr_gap + 1 : curr_gap + window_size + 1]]

            for t in token_table:
                score_dividend += blk_frq(t, b1) * blk_frq(t, b2)
                score_divisor_b1 += blk_frq(t, b1) ** 2
                score_divisor_b2 += blk_frq(t, b2) ** 2
            try:
                score = score_dividend / math.sqrt(score_divisor_b1 * score_divisor_b2)
            except ZeroDivisionError:
                pass  # score += 0.0

            gap_scores.append(score)

        return gap_scores

    def _create_token_table(self, token_sequences, par_breaks):
        "Creates a table of TokenTableFields"
        token_table = {}
        current_par = 0
        current_tok_seq = 0
        pb_iter = par_breaks.__iter__()
        current_par_break = next(pb_iter)
        if current_par_break == 0:
            try:
                current_par_break = next(pb_iter)  # error if nothing more to iterate over, i.e. if no break (par_breaks == [0])
            except StopIteration as e:
                raise ValueError(
                    "No paragraph breaks were found (text too short perhaps?)"
                ) from e
        for ts in token_sequences:
            for word, index in ts.wrdindex_list:
                try:
                    while index > current_par_break:
                        current_par_break = next(pb_iter)
                        current_par += 1
                except StopIteration:
                    # hit bottom
                    pass

                if word in token_table:
                    token_table[word].total_count += 1

                    if token_table[word].last_par != current_par:
                        token_table[word].last_par = current_par
                        token_table[word].par_count += 1

                    if token_table[word].last_tok_seq != current_tok_seq:
                        token_table[word].last_tok_seq = current_tok_seq
                        token_table[word].ts_occurences.append([current_tok_seq, 1])
                    else:
                        token_table[word].ts_occurences[-1][1] += 1
                else:  # new word
                    token_table[word] = TokenTableField(
                        first_pos=index,
                        ts_occurences=[[current_tok_seq, 1]],
                        total_count=1,
                        par_count=1,
                        last_par=current_par,
                        last_tok_seq=current_tok_seq,
                    )

            current_tok_seq += 1

        return token_table

    def _depth_scores(self, scores):
        """Calculates the depth of each gap, i.e. the average difference
        between the left and right peaks and the gap's score"""

        depth_scores = [0 for x in scores]
        # clip boundaries: this holds on the rule of thumb(my thumb)
        # that a section shouldn't be smaller than at least 2
        # pseudosentences for small texts and around 5 for larger ones.

        clip = min(max(len(scores) // 10, 2), 5)
        index = clip

        for gapscore in scores[clip:-clip]:
            lpeak = gapscore
            for score in scores[index::-1]:
                if score >= lpeak:
                    lpeak = score
                else:
                    break
            rpeak = gapscore
            for score in scores[index:]:
                if score >= rpeak:
                    rpeak = score
                else:
                    break
            depth_scores[index] = lpeak + rpeak - 2 * gapscore
            index += 1

        return depth_scores

    def _divide_to_tokensequences(self, text):
        "Divides the text into pseudosentences of fixed size"
        w = self.w
        wrdindex_list = []
        matches = re.finditer(r"\w+", text)
        for match in matches:
            wrdindex_list.append((match.group(), match.start()))
        token_sequences = [
            TokenSequence(int(i / w), wrdindex_list[i : i + w])
            for i in range(0, len(wrdindex_list), w)
        ]
        # for ts in token_sequences[:5]:
        #     print(ts)
        return token_sequences

    def _identify_boundaries(self, depth_scores):
        """Identifies boundaries at the peaks of similarity score
        differences"""

        boundaries = [0 for x in depth_scores]

        avg = sum(depth_scores) / len(depth_scores)
        stdev = numpy.std(depth_scores)

        if self.cutoff_policy == LC:
            cutoff = avg - stdev
        else:
            cutoff = avg - stdev / 2.0

        depth_tuples = sorted(zip(depth_scores, range(len(depth_scores))))
        depth_tuples.reverse()
        hp = list(filter(lambda x: x[0] > cutoff, depth_tuples))

        for dt in hp:
            boundaries[dt[1]] = 1
            for dt2 in hp:  # undo if there is a boundary close already
                if (
                    dt[1] != dt2[1]
                    and abs(dt2[1] - dt[1]) < 4
                    and boundaries[dt2[1]] == 1
                ):
                    boundaries[dt[1]] = 0
        return boundaries

    def _mark_paragraph_breaks(self, text):
        """Identifies indented text or line breaks as the beginning of
        paragraphs"""
        MIN_PARAGRAPH = 1#00
        pattern = re.compile("[ \t\r\f\v]*\n[ \t\r\f\v]*\n[ \t\r\f\v]*")
        matches = pattern.finditer(text)

        last_break = 0
        pbreaks = [0]
        for pb in matches:
            if pb.start() - last_break < MIN_PARAGRAPH:
                continue
            else:
                pbreaks.append(pb.start())
                last_break = pb.start()

        return pbreaks

    def _normalize_boundaries(self, text, boundaries, paragraph_breaks):
        """Normalize the boundaries identified to the original text's
        paragraph breaks"""

        norm_boundaries = []
        char_count, word_count, gaps_seen = 0, 0, 0
        seen_word = False

        for char in text:
            char_count += 1
            if char in " \t\n" and seen_word: # We saw a word and a character that indicates its end (space, new line, etc.)
                seen_word = False
                word_count += 1
            if char not in " \t\n" and not seen_word: # We see a character that belongs to a word
                seen_word = True
            if gaps_seen < len(boundaries) and word_count > (
                max(gaps_seen * self.w, self.w)
            ): # we haven't reached the last gap and we saw enough words to reach a new gap
                if boundaries[gaps_seen] == 1: # if there is a boundary at this gap
                    # find closest paragraph break
                    best_fit = len(text) # the largest distance between a 'false' break and a 'real' one is the length of the text
                    for br in paragraph_breaks: # br is a 'real' break
                        if best_fit > abs(br - char_count): # if we find a break closer than the one we had before
                            best_fit = abs(br - char_count) # we record the distance
                            bestbr = br # we record the new best break
                        else: # if we find a larger distance, we have passed the right break already
                            break
                    if bestbr not in norm_boundaries:  # avoid duplicates
                        norm_boundaries.append(bestbr)
                gaps_seen += 1

        return norm_boundaries

    def _smooth_scores(self, gap_scores, window='flat'):
        "Wraps the smooth function from the SciPy Cookbook"
        return list(
            smooth(numpy.array(gap_scores[:]), window_len=self.smoothing_width + 1, window=window)
        )
    
    def _vocabulary_introduction(self, tokseqs, token_table, memory_size = None):
        """Implements the vocabulary introduction method"""

        def vi_frq(block, memory_size):
            freq = 0
            for ts in block:
                for tok, i in ts.wrdindex_list:
                    seen_words_in_ts = set()
                    if token_table[tok].first_pos == i: # never seen before
                        freq += 1
                    elif memory_size is not None and tok not in seen_words_in_ts:
                        tok_exists_in_ts = {occ[0] for occ in token_table[tok].ts_occurences}
                        if not tok_exists_in_ts.intersection(set(range(ts.index - memory_size, ts.index))):
                            freq += 1
                    seen_words_in_ts.add(tok)
            return freq

        gap_scores = []
        numgaps = len(tokseqs) - 1

        for curr_gap in range(numgaps):
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < self.k - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - self.k:
                window_size = numgaps - curr_gap
            else:
                window_size = self.k

            b1 = tokseqs[curr_gap - window_size + 1 : curr_gap + 1]
            b2 = tokseqs[curr_gap + 1 : curr_gap + window_size + 1]

            len_b1 = sum([ts.original_length for ts in b1])
            len_b2 = sum([ts.original_length for ts in b2])

            score = (vi_frq(b1, memory_size) + vi_frq(b2, memory_size))/(len_b1 + len_b2)

            gap_scores.append(score)

        return gap_scores



class TokenTableField:
    """A field in the token table holding parameters for each token,
    used later in the process"""

    def __init__(
        self,
        first_pos,
        ts_occurences,
        total_count=1,
        par_count=1,
        last_par=0,
        last_tok_seq=None,
    ):
        self.__dict__.update(locals())
        del self.__dict__["self"]

    def __repr__(self):
        return f"first_pos = {self.first_pos}, ts_occurences = {self.ts_occurences}, total_count = {self.total_count}, par_count = {self.par_count}, last_par = {self.last_par}, last_tok_seq = {self.last_tok_seq}"

    def __str__(self):
        return f"first_pos = {self.first_pos}, ts_occurences = {self.ts_occurences}, total_count = {self.total_count}, par_count = {self.par_count}, last_par = {self.last_par}, last_tok_seq = {self.last_tok_seq}"



class TokenSequence:
    "A token list with its original length and its index"

    def __init__(self, index, wrdindex_list, original_length=None):
        original_length = original_length or len(wrdindex_list)
        self.__dict__.update(locals())
        del self.__dict__["self"]

    def __repr__(self):
        return f"{self.index} ({self.original_length}): {str(self.wrdindex_list)}"

    def __str__(self):
        return f"{self.index} ({self.original_length}): {str(self.wrdindex_list)}"



# Pasted from the SciPy cookbook: https://www.scipy.org/Cookbook/SignalSmooth
def smooth(x, window_len=11, window="flat"):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the beginning and end part of the output signal.

    :param x: the input signal
    :param window_len: the dimension of the smoothing window; should be an odd integer
    :param window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
        flat window will produce a moving average smoothing.

    :return: the smoothed signal

    example::

        t=linspace(-2,2,0.1)
        x=sin(t)+randn(len(t))*0.1
        y=smooth(x)

    :see also: numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve,
        scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    if window not in ["flat", "hanning", "hamming", "bartlett", "blackman"]:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
        )

    s = numpy.r_[2 * x[0] - x[window_len:1:-1], x, 2 * x[-1] - x[-1:-window_len:-1]]

    # print(len(s))
    if window == "flat":  # moving average
        w = numpy.ones(window_len, "d")
    else:
        w = eval("numpy." + window + "(window_len)")

    y = numpy.convolve(w / w.sum(), s, mode="same")

    return y[window_len - 1 : -window_len + 1]


def demo(text, gold, params={}):

    # Get XP topics
    tt = TextTilingTokenizer(**params)
    _, norm_b = tt.tokenize(text)

    pr = precision_score(gold, norm_b)
    rec = recall_score(gold, norm_b)
    f1 = f1_score(gold, norm_b)
    pr2, rec2, f2 = utils.Fk(gold, norm_b, k=2)
    pk = seg_mod.pk(gold, norm_b, boundary=1)

    return pr, rec, f1, pr2, rec2, f2, pk, norm_b


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to create a topic segmentation of some episodes of Friends based on the original Friends algorithm.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-x", "--experience-name", type=str, default='TextTilingOG', help="name of the experiment")
    parser.add_argument("-s", "--season", nargs='+', type=int, default=[1,2,3,4,5,6,7,8,9,10], help="season(s) we train on")
    parser.add_argument("-p", "--generate-pdf", type=bool, default=False, help="to generate a pdf of the results")
    args = parser.parse_args()
    config = vars(args)

    xp_name = config["experience_name"]
    seasons = config["season"]

    dataset = readFriends.FriendsDataset()

    params = {
        'w': 12,
        'k': 10,
        'memory_size': 20,
        'similarity_method': BLOCK_COMPARISON,
        'stopwords': None,
        'smoothing_method': DEFAULT_SMOOTHING,
        'smoothing_window': FLAT_WINDOW,
        'smoothing_width': 2,
        'smoothing_rounds': 1,
        'cutoff_policy': HC,
    }
    
    all_f1 = []
    all_fk1 = []
    all_fk2 = []
    all_pk = []
    all_ep = []

    for season in seasons:
        num_ep = dataset.dataset_stats['Episodes'].loc[str(season)]
        for ep in range(1, num_ep + 1):
            all_ep.append(f's{season}e{ep}')
            df = dataset.as_df(dataset.get_episode(season, ep))
            # Gold seg
            segmentation_gold = []
            for _, l in itertools.groupby(df['topics_scenes_and_notes']):
                segmentation_gold += [0]*(sum(1 for _ in l)-1) + [1]
            
            # Get XP topics
            tt = TextTilingTokenizer(**params)
            _, norm_b = tt.tokenize('\n\n'.join(df['utterance']))

            pr = precision_score(segmentation_gold[:-1], norm_b)
            rec = recall_score(segmentation_gold[:-1], norm_b)
            f1 = f1_score(segmentation_gold[:-1], norm_b)
            prk1, reck1, fk1 = utils.Fk(segmentation_gold[:-1], norm_b, k=1)
            prk2, reck2, fk2 = utils.Fk(segmentation_gold[:-1], norm_b, k=2)
            pk = seg_mod.pk(segmentation_gold[:-1], norm_b, boundary=1)

            all_f1.append(f1)
            all_fk1.append(fk1)
            all_fk2.append(fk2)
            all_pk.append(pk)
            
            print(f"Season {season}, Episode {ep}")

        print(f"Season {season} OK\n")

    # Save results for this XP
    df = pd.DataFrame([all_f1, all_fk1, all_fk2, all_pk], index=['F1', 'Fk1', 'Fk2', 'Pk'], columns=all_ep)
    df.to_csv(f"../results/{xp_name}.csv")
    