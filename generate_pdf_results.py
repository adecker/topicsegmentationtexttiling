import os, glob
import re
import argparse
import pandas as pd
from pylatex import Document, Section, NoEscape, Package, Subsection, Subsubsection, NewPage, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat, Command, Itemize

def ep_in_valid_season(ep, seasons):
    m = int(re.match(r"s(\d+)e\d+", ep).group(1))
    return m if m in seasons else None

def generate_pdf(xp_name, seasons = None, clean = True):
    geometry_options = {"tmargin": ".5cm", "lmargin": ".5cm", "bmargin": ".5cm", "rmargin": ".5cm"}
    doc = Document(geometry_options=geometry_options)

    doc.packages.append(Package('xcolor'))
    doc.packages.append(Package('colortbl'))
    doc.packages.append(Package('multirow'))
    doc.packages.append(Package('booktabs'))

    df = pd.read_csv(f'./results/{xp_name}.csv',index_col=0).T
    if seasons is None: # we want the results for all the episodes and seasons in the files
        all_keys = df.index
        seasons = sorted(list(set([int(s) for s in re.findall(r"s(\d+)", ', '.join(all_keys))])))
    
    # Average per season
    avg_df = df.groupby(lambda s:  ep_in_valid_season(s, seasons)).mean()
    avg_df.loc['Avg'] = avg_df.mean()
    # Keep only styler = avg_df.style to remove the colouring or if your measures are different
    styler = avg_df.style.background_gradient(cmap = 'RdYlGn', subset=['F1'], vmax=1, vmin=0)\
                    .background_gradient(cmap = 'RdYlGn', subset=['Fk1'], vmax=1, vmin=0)\
                    .background_gradient(cmap = 'RdYlGn', subset=['Fk2'], vmax=1, vmin=0)\
                    .background_gradient(cmap = 'RdYlGn_r', subset=['Pk'], vmax=1, vmin=0)
    table = styler.to_latex(position='h!', position_float='centering', hrules=True, multicol_align='c', convert_css=True, caption = f"Average Results of All Seasons")
    table_lines = table.splitlines()
    table_lines.insert(len(table_lines)-4, '\midrule')
    table = '\n'.join(table_lines)
    doc.append(NoEscape(table))

    # All the results per episode for each season
    for s in seasons:
        df_s = df.loc[[f's{s}e' in n for n in df.index]].copy()
        df_s.loc['Avg'] = df_s.mean()
        # Keep only styler = avg_df.style to remove the colouring or if your measures are different
        styler = df_s.rename(index = lambda n: re.sub(r"s\d+e(\d+)", r"Episode \1", n))\
                        .style.background_gradient(cmap = 'RdYlGn', subset=['F1'], vmax=1, vmin=0)\
                        .background_gradient(cmap = 'RdYlGn', subset=['Fk1'], vmax=1, vmin=0)\
                        .background_gradient(cmap = 'RdYlGn', subset=['Fk2'], vmax=1, vmin=0)\
                        .background_gradient(cmap = 'RdYlGn_r', subset=['Pk'], vmax=1, vmin=0)
        table = styler.to_latex(position='h!', position_float='centering', hrules=True, multicol_align='c', convert_css=True, caption = f"Average Results of Season {s}")
        table_lines = table.splitlines()
        table_lines.insert(len(table_lines)-4, '\midrule')
        table = '\n'.join(table_lines)
        doc.append(NoEscape(table))

    doc.generate_tex(f'./results/pdf/{xp_name}')
    doc.generate_pdf(f'./results/pdf/{xp_name}', clean_tex=clean)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to generate a pdf based on the results of the TextTiling algorithm.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-x", "--experience-name", type=str, default='test', help="name of the experiment we want to create a pdf for")
    parser.add_argument("-s", "--season", nargs='+', type=int, default=None, help="season(s) to display in our pdf")
    parser.add_argument("-c", "--clean", type=bool, default=True, help="clean the tex file created when generating the pdf")
    args = parser.parse_args()
    config = vars(args)

    xp_name = config["experience_name"]
    seasons = config["season"]
    clean = config["clean"]
    generate_pdf(xp_name, seasons = seasons, clean = clean)