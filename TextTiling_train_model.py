# Small adaptations from the code of Linzi Xing and Giuseppe Carenini:
# Improving Unsupervised Dialogue Topic Segmentation with Utterance-Pair Coherence Scoring
# https://github.com/lxing532/Dialogue-Topic-Segmenter
# https://aclanthology.org/2021.sigdial-1.18.pdf
#
# Modifications by: Authors of the paper

import argparse
from transformers import BertTokenizer
import torch
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertForNextSentencePrediction
from sklearn.model_selection import train_test_split
from transformers import get_linear_schedule_with_warmup
import torch.nn as nn
import numpy
from itertools import combinations
import argparse
from datetime import datetime


def MarginRankingLoss(p_scores, n_scores):
    margin = 1 #0.1, 0.5, 1, 2, 5
    scores = margin - p_scores + n_scores
    scores = scores.clamp(min=0)

    return scores.mean()

parser = argparse.ArgumentParser(description="Script to train the similarity score model.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-x", "--experience-name", type=str, default='e', help="name of the experiment")
parser.add_argument("-e", "--epochs", type=int, default=10, help="number of training epochs")
parser.add_argument("-b", "--batch-size", type=int, default=32, help="size of the data batches")
args = parser.parse_args()
config = vars(args)
print(config)

xp_name = config["experience_name"]
epochs = config["epochs"]
batch_size = config["batch_size"]

#device = 0
device = "cuda" if torch.cuda.is_available() else "cpu"
device = torch.device(device)
print(f"\nThe device is: {device}")

# Load the BERT tokenizer.
print(f'\nLoading BERT tokenizer... ({datetime.now().strftime("%H:%M:%S")})')
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)

sample_num_memory = []
id_inputs = []

print(f'Loading data... ({datetime.now().strftime("%H:%M:%S")})')
# check if the data is already saved as tensors
try:
    train_inputs = torch.load(f'./training_data/train_inputs_{xp_name}.pt')
    validation_inputs = torch.load(f'./training_data/validation_inputs_{xp_name}.pt')
    train_labels = torch.load(f'./training_data/train_labels_{xp_name}.pt')
    validation_labels = torch.load(f'./training_data/validation_labels_{xp_name}.pt')
    train_masks = torch.load(f'./training_data/train_masks_{xp_name}.pt')
    validation_masks = torch.load(f'./training_data/validation_masks_{xp_name}.pt')
    print(f'\tData loaded from saved tensors. ({datetime.now().strftime("%H:%M:%S")})')
except:
    #for line in open('/Users/linzi/Desktop/dialogue_test/training_data/dailydial/dailydial_sample_num.txt'):
    for line in open(f'./training_data/friends_sample_num_{xp_name}.txt'):
        line = line.strip()
        sample_num_memory.append(int(line))

    i = 1
    for line in open(f'./training_data/friends_pairs_{xp_name}.txt'):
        line = line.strip().split('\t\t')
        sent1 = line[0]
        sent2 = line[1]
        encoded_sent1 = tokenizer.encode(sent1, add_special_tokens = True, truncation = True, max_length = 128, return_tensors = 'pt')
        encoded_sent2 = tokenizer.encode(sent2, add_special_tokens = True, truncation = True, max_length = 128, return_tensors = 'pt')
        encoded_pair = encoded_sent1[0].tolist() + encoded_sent2[0].tolist()[1:]
        id_inputs.append(torch.Tensor(encoded_pair))
        if not i%10000:
            print(f'\t{i} steps done ({datetime.now().strftime("%H:%M:%S")})')
        i += 1
        # if i > 100:
        #     break

    MAX_LEN = 256
    print(f'Padding/truncating all sentences to {MAX_LEN} values... ({datetime.now().strftime("%H:%M:%S")})')

    id_inputs = torch.nn.utils.rnn.pad_sequence(id_inputs + [torch.zeros(MAX_LEN)], batch_first = True)[:-1].tolist()

    attention_masks = []
    for sent in id_inputs:
        att_mask = [int(token_id > 0) for token_id in sent]
        attention_masks.append(att_mask)

    # group samples .....
    grouped_inputs = []; grouped_masks = []
    count = 0
    for i in sample_num_memory:
        grouped_inputs.append(id_inputs[count: count+i])
        grouped_masks.append(attention_masks[count: count+i])
        count = count + i

    print(f'\nThe group number is: {str(len(grouped_inputs))} ({datetime.now().strftime("%H:%M:%S")})')
    # generate pos/neg pairs ....
    print(f'start generating pos and neg pairs ...  ({datetime.now().strftime("%H:%M:%S")})')
    pos_neg_pairs = []; pos_neg_masks = []

    for i in range(len(grouped_inputs)):#
        #print(len(grouped_inputs[i]))
        if len(grouped_inputs[i]) == 2:
            pos_neg_pairs.append(grouped_inputs[i]) # + -
            pos_neg_masks.append(grouped_masks[i])
        else:
            all_pairs = list(combinations(range(len(grouped_inputs[i])), 2))
            #print(all_pairs)
            for i1, i2 in all_pairs:
                pos_neg_pairs.append([grouped_inputs[i][i1], grouped_inputs[i][i2]])
                pos_neg_masks.append([grouped_masks[i][i1], grouped_masks[i][i2]])

    print(f'{str(len(pos_neg_pairs))} samples were generated... ({datetime.now().strftime("%H:%M:%S")})\n')
    fake_labels = [0]*len(pos_neg_pairs)

    train_inputs, validation_inputs, train_labels, validation_labels = train_test_split(pos_neg_pairs, fake_labels, random_state=2018, train_size=0.8)
    # Do the same for the masks.
    train_masks, validation_masks, _, _ = train_test_split(pos_neg_masks, fake_labels, random_state=2018, train_size=0.8)

    train_inputs = torch.tensor(train_inputs)
    torch.save(train_inputs, f'./training_data/train_inputs_{xp_name}.pt')
    validation_inputs = torch.tensor(validation_inputs)
    torch.save(validation_inputs, f'./training_data/validation_inputs_{xp_name}.pt')
    train_labels = torch.tensor(train_labels)
    torch.save(train_labels, f'./training_data/train_labels_{xp_name}.pt')
    validation_labels = torch.tensor(validation_labels)
    torch.save(validation_labels, f'./training_data/validation_labels_{xp_name}.pt')
    train_masks = torch.tensor(train_masks)
    torch.save(train_masks, f'./training_data/train_masks_{xp_name}.pt')
    validation_masks = torch.tensor(validation_masks)
    torch.save(validation_masks, f'./training_data/validation_masks_{xp_name}.pt')

    
finally:
    train_data = TensorDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)
    print(f"\nSize of training dataloader with batch size of {batch_size}: {len(train_dataloader)}")

    # Create the DataLoader for our validation set.
    validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)
    print(f"Size of validation dataloader with batch size of {batch_size}: {len(validation_dataloader)}\n")

# The model
coherence_prediction_decoder = []
coherence_prediction_decoder.append(nn.Linear(768, 768))
coherence_prediction_decoder.append(nn.ReLU())
coherence_prediction_decoder.append(nn.Dropout(p=0.1))
coherence_prediction_decoder.append(nn.Linear(768, 2))
coherence_prediction_decoder = nn.Sequential(*coherence_prediction_decoder)
coherence_prediction_decoder.to(device)

model = BertForNextSentencePrediction.from_pretrained("bert-base-uncased", num_labels = 2, output_attentions = False, output_hidden_states = True)
#model.cuda(device)
model.to(device)
optimizer = torch.optim.AdamW(list(model.parameters())+list(coherence_prediction_decoder.parameters()), lr = 2e-5, eps = 1e-8) #AdamW

# Total number of training steps is number of batches * number of epochs.
total_steps = len(train_dataloader) * epochs
# Create the learning rate scheduler.
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps = 0, num_training_steps = total_steps)


for epoch_i in range(0, epochs):

    total_loss = 0

    print("")
    print(f'======== Epoch {epoch_i + 1} / {epochs} ======== ({datetime.now().strftime("%H:%M:%S")})')
    print('Training...')

    total_loss = 0

    model.train()
    coherence_prediction_decoder.train()

    for step, batch in enumerate(train_dataloader):

        if step % 1000 == 0 and not step == 0:
            print(f'{str(step)} steps done.... ({datetime.now().strftime("%H:%M:%S")})')

        b_input_ids = batch[0].to(device).long()
        b_input_mask = batch[1].to(device).int()
        model.zero_grad()
        coherence_prediction_decoder.zero_grad()

        pos_scores = model(b_input_ids[:,0,:], attention_mask=b_input_mask[:,0,:])
        pos_scores = pos_scores[1][-1][:,0,:]
        pos_scores = coherence_prediction_decoder(pos_scores)

        neg_scores = model(b_input_ids[:,1,:], attention_mask=b_input_mask[:,1,:])
        neg_scores = neg_scores[1][-1][:,0,:]
        neg_scores = coherence_prediction_decoder(neg_scores)

        #loss = MarginRankingLoss(pos_scores[0][:,0], neg_scores[0][:,0])
        loss = MarginRankingLoss(pos_scores[:,0], neg_scores[:,0]) # loss that says that pos_scores should be ranked higher than neg_scores
        # since we have examples +/-;+/--;-/--, it learns all the differences we want it to learn

        total_loss += loss.item()
        loss.backward()
        
        torch.nn.utils.clip_grad_norm_(list(model.parameters())+list(coherence_prediction_decoder.parameters()), 1.0)
        optimizer.step()
        scheduler.step()

    avg_train_loss = total_loss / len(train_dataloader)
    print(f'=========== the loss for epoch {str(epoch_i)} is: {str(avg_train_loss)} ({datetime.now().strftime("%H:%M:%S")})')

    print("")
    print(f'Running Validation... ({datetime.now().strftime("%H:%M:%S")})')

    model.eval()
    coherence_prediction_decoder.eval()

    all_pos_scores = []
    all_neg_scores = []

    for step, batch in enumerate(validation_dataloader):

        if step % 1000 == 0 and not step == 0:
            print(f'{str(step)} steps done.... ({datetime.now().strftime("%H:%M:%S")})')

        b_input_ids = batch[0].to(device).long()
        b_input_mask = batch[1].to(device).int()

        with torch.no_grad():
            pos_scores = model(b_input_ids[:,0,:], attention_mask=b_input_mask[:,0,:])
            pos_scores = pos_scores[1][-1][:,0,:]
            pos_scores = coherence_prediction_decoder(pos_scores)
            neg_scores = model(b_input_ids[:,1,:], attention_mask=b_input_mask[:,1,:])
            neg_scores = neg_scores[1][-1][:,0,:]
            neg_scores = coherence_prediction_decoder(neg_scores)

        all_pos_scores += pos_scores[:,0].detach().cpu().numpy().tolist()
        all_neg_scores += neg_scores[:,0].detach().cpu().numpy().tolist()

    labels = []

    for i in range(len(all_pos_scores)):
        if all_pos_scores[i] > all_neg_scores[i]:
            labels.append(1)
        else:
            labels.append(0)

    print(sum(labels)/float(len(all_pos_scores)))

    
    # PATH = f'./model/bert_{str(epoch_i)}'
    # torch.save(model.state_dict(), PATH)
    model.save_pretrained(f'./trained_bert/model_{xp_name}/m{str(epoch_i)}/')
    tokenizer.save_pretrained(f'./trained_bert/tokenizer_{xp_name}/t{str(epoch_i)}/')


























