# Natural Language Toolkit: TextTiling
#
# Copyright (C) 2001-2023 NLTK Project
# Author: George Boutsioukis
#
# URL: <https://www.nltk.org/>
# For license information, see LICENSE.TXT
#
# Modifications by: Authors of the paper

import math
import re
import pandas as pd
import numpy as np
from nltk.metrics import segmentation as seg_mod
import spacy
import argparse
import torch

from sklearn.metrics import f1_score, precision_score, recall_score

from pylatex import Document, Section, NoEscape, Package, Subsection, Subsubsection, NewPage, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat, Command, Itemize

import utils

try:
    import numpy
except ImportError:
    pass

from nltk.tokenize.api import TokenizerI

BC, BC_COREF, VI, BC_VI, BC_COREF_VI= 2, 3, 5, 10, 15
SPK_DEPTH, SPK_INTRO = 1, 2
LC, HC = 0, 1
DEFAULT_SMOOTHING = [0]
FLAT_WINDOW, HANNING_WINDOW, HAMMING_WINDOW, BARTLETT_WINDOW, BLACKMAN_WINDOW = "flat", "hanning", "hamming", "bartlett", "blackman"


class TextTilingTokenizer(TokenizerI):
    """Tokenize a document into topical sections using the TextTiling algorithm.
    This algorithm detects subtopic shifts based on the analysis of lexical
    co-occurrence patterns.

    The process starts by tokenizing the text into pseudosentences of
    a fixed size w. Then, depending on the method used, similarity
    scores are assigned at sentence gaps. The algorithm proceeds by
    detecting the peak differences between these scores and marking
    them as boundaries. The boundaries are normalized to the closest
    paragraph break and the segmented text as well as the list of 
    boundaries are returned.

    :param w: Pseudosentence size
    :type w: int
    :param k: Size (in sentences) of the block used in the block comparison method
    :type k: int
    :param memory_size: Size (in sentences) of the memory used in the vocabulary introduction method
    :type memory_size: int
    :param similarity_method: The method used for determining similarity scores:
       `BLOCK_COMPARISON` (default) or `VOCABULARY_INTRODUCTION`.
    :type similarity_method: constant
    :param stopwords: A list of stopwords that are filtered out (defaults to NLTK's stopwords corpus)
    :type stopwords: list(str)
    :param smoothing_method: The method used for smoothing the score plot:
      `DEFAULT_SMOOTHING` (default)
    :type smoothing_method: constant
    :param smoothing_window: The type of window used for smoothing the score plot:
      `FLAT_WINDOW` (default), HANNING_WINDOW, HAMMING_WINDOW, BARTLETT_WINDOW or BLACKMAN_WINDOW
    :type smoothing_window: constant
    :param smoothing_width: The width of the window used by the smoothing method
    :type smoothing_width: int
    :param smoothing_rounds: The number of smoothing passes
    :type smoothing_rounds: int
    :param cutoff_policy: The policy used to determine the number of boundaries:
      `HC` (default) or `LC`
    :type cutoff_policy: constant
    :param stemmer: Whether to use a stemmer or not
    :type stemmer: bool
    :param lemmatizer: Whether to use a lemmatizer or not
    :type lemmatizer: bool
    :param speaker_method: The method used to include the speakers as a feature to identify topic shifts:
      `SPK_DEPTH` (default), `SPK_INTRO` or None
    :type speaker_method: constant
    :param questions: Whether to use questions as a feature to identify topic shifts or not
    :type questions: bool
    """

    def __init__(
        self,
        w=12,
        k=20,
        memory_size=20,
        similarity_method=BC,
        stopwords=None,
        smoothing_method=DEFAULT_SMOOTHING,
        smoothing_window=FLAT_WINDOW,
        smoothing_width=2,
        smoothing_rounds=1,
        cutoff_policy=HC,
        stemmer=False,
        lemmatizer=False,
        speaker_method=SPK_DEPTH,
        questions=False,
    ):

        if stopwords is None:
            from nltk.corpus import stopwords

            stopwords = stopwords.words("english")
        if stemmer:
            from nltk.stem.snowball import SnowballStemmer

            stemmer = SnowballStemmer("english")
        if lemmatizer:
           from nltk.stem import WordNetLemmatizer

           lemmatizer = WordNetLemmatizer()
        self.__dict__.update(locals())
        del self.__dict__["self"]


    def tokenize(self, text, speakers):
        """Return a tokenized copy of *text*, where each "token" represents
        a separate topic."""

        lowercase_text = text.lower()

        speakers_list_full, all_speakers = self._make_speakers_list(speakers)

        # Tokenization step starts here

        # Remove punctuation
        nopunct_text = "".join(
            c for c in lowercase_text if re.match(r"[a-z\-'? \n\t]", c)
        )
        nopunct_par_breaks = self._mark_paragraph_breaks(nopunct_text)

        tokseqs, utterances, tokenized_utterances, num_joined_utterances = self._divide_to_tokensequences(nopunct_text)
        # Merge some speakers list to match token sequences
        speakers_list = []
        i = 0
        for n in num_joined_utterances:
            if n > 1:
                speakers_list.append(sum(speakers_list_full[i:i+n], []))
            else:
                speakers_list.append(speakers_list_full[i])
            i += n

        # Filter stopwords
        for ts in tokseqs:
            ts.wrdindex_list = [
                wi for wi in ts.wrdindex_list if wi[0] not in self.stopwords
            ]

        '''A comment in the original C implementation of TextTiling
        states that stemming offers no benefit to the process. Our
        expertiments on the Friends dataset confirmed this.'''
        if self.stemmer:
            for ts in tokseqs:
                ts.wrdindex_list = self._stem_words(ts.wrdindex_list, stemmer=self.stemmer)
        if self.lemmatizer:
            for ts in tokseqs:
                ts.wrdindex_list = self._lemmatize_words(ts.wrdindex_list, lemmatizer=self.lemmatizer)

        token_table = self._create_token_table(tokseqs, nopunct_par_breaks)
        # End of the Tokenization step

        # Lexical score determination
        if self.similarity_method == BC:
            gap_scores = self._block_comparison(tokseqs, token_table)
        elif self.similarity_method == BC_COREF:
            gap_scores = self._block_comparison_with_coref(tokseqs, token_table, utterances)
        elif self.similarity_method == VI:
            gap_scores = self._vocabulary_introduction(tokseqs, token_table, self.memory_size)
        elif self.similarity_method == BC_VI:
            gap_scores_block = self._block_comparison(tokseqs, token_table)
            gap_scores_voc = self._vocabulary_introduction(tokseqs, token_table, self.memory_size)
            gap_scores = [(gap_scores_block[i] + gap_scores_voc[i])/2 for i in range(len(gap_scores_voc))]
        elif self.similarity_method == BC_COREF_VI:
            gap_scores_block = self._block_comparison_with_coref(tokseqs, token_table, utterances)
            gap_scores_voc = self._vocabulary_introduction(tokseqs, token_table, self.memory_size)
            gap_scores = [(gap_scores_block[i] + gap_scores_voc[i])/2 for i in range(len(gap_scores_voc))]
        else:
            raise ValueError(
                f"Similarity method {self.similarity_method} not recognized"
            )
        if self.questions: ## Add questions scores
            for i, u in enumerate(utterances[:-1]):
                if u.endswith('?'):
                    if i == 0: # ??
                        gap_scores[i] = gap_scores[i+1]
                    elif i == len(gap_scores) - 1: # ??
                        gap_scores[i] = gap_scores[i-1]
                    else: 
                        gap_scores[i] = (gap_scores[i-1]+gap_scores[i+1])/2

        if self.smoothing_method == DEFAULT_SMOOTHING:
            smooth_scores = self._smooth_scores(gap_scores, self.smoothing_window)
        else:
            raise ValueError(f"Smoothing method {self.smoothing_method} not recognized")
        # End of Lexical score Determination

        # Boundary identification
        ## comparison method + questions
        depth_scores = self._depth_scores(smooth_scores)

        ## speakers: one list of depth scores per speaker
        speakers_depth_scores = self._speaker_scores(speakers_list, all_speakers)
        speakers_introductions = self._speaker_intro(speakers_list)

        if self.speaker_method == SPK_INTRO:
            ### new speaker introduction
            for i, x in enumerate(speakers_introductions):
                if x:
                    if i:
                        depth_scores[i-1] = (0.01 + depth_scores[i-1])*1.3
                    depth_scores[i] = (0.01 + depth_scores[i])*1.3
                    if i<len(speakers_introductions)-1:
                        depth_scores[i+1] = (0.01 + depth_scores[i+1])*1.3
            segment_boundaries = self._identify_boundaries(depth_scores)
        elif self.speaker_method == SPK_DEPTH:
            ### by depth
            n = int(len(speakers_depth_scores)/2)
            m = len(speakers_depth_scores) + n
            depth_scores = [sum(x)/m for x in zip(*speakers_depth_scores, *n*[depth_scores])]
            ### by boundaries
            speakers_boundaries = [self._identify_boundaries(d) for d in speakers_depth_scores]
            text_boundaries = self._identify_boundaries(depth_scores)
            n = 2*int(len(speakers_depth_scores))
            m = (len(speakers_depth_scores) + n)/3
            segment_boundaries = [1 if x > m else 0 for x in [sum(i) for i in zip(*speakers_boundaries, *n*[text_boundaries])]]
        else: # ignore speakers
            segment_boundaries = self._identify_boundaries(depth_scores)
        
        # End of Boundary Identification
        segmented_text = []

        topic = [utterances[0]]
        for i, b in enumerate(segment_boundaries):
            if b:
                segmented_text.append('\n\n'.join(topic))
                topic = [utterances[i+1]]
            else:
                topic.append(utterances[i+1])

        if segment_boundaries[-1]: # if 1, last utterance alone as topic
            segmented_text.append(utterances[-1])
        else: # if 0, last topic not included yet
            segmented_text.append('\n\n'.join(topic))

        if not segmented_text: # if no segment yet, return the whole text, ie. 1 single topic
            segmented_text = [text]

        return segmented_text, segment_boundaries
    
    def count_speaker_turns(self, speakers_list, all_speakers, window = 5):
        numgaps = len(speakers_list) - 1
        df = pd.DataFrame(np.nan, index = range(numgaps), columns = [f'{s}_{lim}' for lim in ['up_to_curr', 'after_curr'] for s in all_speakers])

        for curr_gap in range(numgaps):
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < window - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - window:
                window_size = numgaps - curr_gap
            else:
                window_size = window
            b1 = speakers_list[curr_gap - window_size + 1 : curr_gap] # .loc includes the right boundary
            b2 = speakers_list[curr_gap + 1 : curr_gap + window_size]

            for s in all_speakers:
                df.at[curr_gap, f'{s}_up_to_curr'] = sum([speakers.count(s) for speakers in b1]) / window_size
                df.at[curr_gap, f'{s}_after_curr'] = sum([speakers.count(s) for speakers in b2]) / window_size

        return df

    def _block_comparison(self, tokseqs, token_table):
        """Implements the block comparison method"""

        def blk_frq(tok, block):
            ts_occs = filter(lambda o: o[0] in block, token_table[tok].ts_occurences) # keeps only the elements of token_table[tok].ts_occurences if the index is in block
            freq = sum(tsocc[1] for tsocc in ts_occs)/len(block) # total number of occurences in all the token sequences
            return freq

        gap_scores = []
        numgaps = len(tokseqs) - 1

        for curr_gap in range(numgaps):
            score_dividend, score_divisor_b1, score_divisor_b2 = 0.0, 0.0, 0.0
            score = 0.0
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < self.k - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - self.k:
                window_size = numgaps - curr_gap
            else:
                window_size = self.k

            b1 = [ts.ts_index for ts in tokseqs[curr_gap - window_size + 1 : curr_gap + 1]]
            b2 = [ts.ts_index for ts in tokseqs[curr_gap + 1 : curr_gap + window_size + 1]]

            for t in token_table:
                score_dividend += blk_frq(t, b1) * blk_frq(t, b2)
                score_divisor_b1 += blk_frq(t, b1) ** 2
                score_divisor_b2 += blk_frq(t, b2) ** 2
            try:
                score = score_dividend / math.sqrt(score_divisor_b1 * score_divisor_b2)
            except ZeroDivisionError:
                pass  # score += 0.0

            gap_scores.append(score)

        return gap_scores

    def _block_comparison_with_coref(self, tokseqs, token_table, utterances):
        """Implements the block comparison method"""

        def blk_frq(tok, block):
            ts_occs = filter(lambda o: o[0] in block, token_table[tok].ts_occurences) # keeps only the elements of token_table[tok].ts_occurences if the index is in block
            freq = sum(tsocc[1] for tsocc in ts_occs)/len(block) # total number of occurences in all the token sequences
            return freq
        nlp = spacy.load('en_core_web_sm')
        nlp.add_pipe('coreferee')

        gap_scores = []
        even_gap = []
        numgaps = len(tokseqs) - 1

        for curr_gap in range(numgaps):
            score_dividend, score_divisor_b1, score_divisor_b2 = 0.0, 0.0, 0.0
            score = 0.0
            must_even_gap = 0
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < self.k - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - self.k:
                window_size = numgaps - curr_gap
            else:
                window_size = self.k

            # compute similarity score
            b1 = [ts.ts_index for ts in tokseqs[curr_gap - window_size + 1 : curr_gap + 1]]
            b2 = [ts.ts_index for ts in tokseqs[curr_gap + 1 : curr_gap + window_size + 1]]

            for t in token_table:
                score_dividend += blk_frq(t, b1) * blk_frq(t, b2)
                score_divisor_b1 += blk_frq(t, b1) ** 2
                score_divisor_b2 += blk_frq(t, b2) ** 2
            try:
                score = score_dividend / math.sqrt(score_divisor_b1 * score_divisor_b2)
            except ZeroDivisionError:
                pass  # score += 0.0
            
            gap_scores.append(score)
            
            # check if this similarity score should be significantly different from neighbours
            t1 = [utterances[i] for i in b1]
            t2 = [utterances[i] for i in b2]
            text = ' '.join(t1 + t2)
            first_token_in_t2 = len(nlp(' '.join(t1)))
            doc = nlp(text)
            for x in doc._.coref_chains:
                ref = [m.root_index for m in x.mentions]
                if min(ref) < first_token_in_t2 and max(ref) >= first_token_in_t2:
                    must_even_gap = 1
                    break
            even_gap.append(must_even_gap)
            
        for i, x in enumerate(even_gap):
            if x:
                if not i: # ??
                    gap_scores[i] = gap_scores[i+1]
                elif i == len(gap_scores) - 1: # ??
                    gap_scores[i] = gap_scores[i-1]
                else: 
                    gap_scores[i] = (gap_scores[i-1]+gap_scores[i+1])/2

        return gap_scores

    def _make_speakers_list(speakers, all_token = "#ALL#"):
        speakers_list = []
        for spks in speakers:
            if not isinstance(spks, str):
                if np.isnan(spks):
                    speakers_list.append([])
                else:
                    raise TypeError("A string of the form 'speaker1, speaker2, ...' is expected, or nan if there is no speaker.")
            elif spks == all_token:
                speakers_list.append(-1)
            else:
                speakers_list.append(spks.split(', '))

        all_speakers = set().union(*[set(l) for l in speakers_list if l != -1])
        all_speakers.discard(all_token)

        speakers_list = [s if s != -1 else list(all_speakers) for s in speakers_list]
        return speakers_list, all_speakers

    def _speaker_scores(self, speakers_list, all_speakers):
        df_speakers = self.count_speaker_turns(speakers_list, all_speakers, window = self.k)
        all_depth_scores = []
        for s in all_speakers:
            before = list(df_speakers[f'{s}_up_to_curr'])
            after = list(df_speakers[f'{s}_after_curr'])
            print(before, '\n***\n', after)
            scores = [abs(a - b) for a,b in zip(after, before)]
            depth_scores = self._depth_scores(scores)
            all_depth_scores.append(depth_scores)
        return all_depth_scores
    
    def _speaker_scores(self, speakers_list, all_speakers):
        df_speakers = self.count_speaker_turns(speakers_list, all_speakers, window = self.memory_size)
        all_depth_scores = []
        for s in all_speakers:
            before = list(df_speakers[f'{s}_up_to_curr'])
            after = [s in speaker_list for speaker_list in speakers_list[1:]]
            scores = [False]*self.memory_size + [(not b) and a for b,a in zip(before[self.memory_size:], after[self.memory_size:])]
            #depth_scores = self._depth_scores(scores)
            all_depth_scores.append(scores)
        return all_depth_scores
    
    def _speaker_intro(self, speakers_list):
        numgaps = len(speakers_list) - 1
        all_speaker_intro = [False] * numgaps
        for curr_gap in range(self.k, len(all_speaker_intro)):
            if curr_gap < self.memory_size - 1:
                window_size = curr_gap + 1
            else:
                window_size = self.memory_size
            before = sum(speakers_list[curr_gap - window_size + 1 : curr_gap], [])
            if not set(before).intersection(set(speakers_list[curr_gap])):
                all_speaker_intro[curr_gap] = True
        return all_speaker_intro

    def _create_token_table(self, token_sequences, par_breaks):
        "Creates a table of TokenTableFields"
        token_table = {}
        current_par = 0
        current_tok_seq = 0
        pb_iter = par_breaks.__iter__()
        current_par_break = next(pb_iter)
        if current_par_break == 0:
            try:
                current_par_break = next(pb_iter)  # error if nothing more to iterate over, i.e. if no break (par_breaks == [0])
            except StopIteration as e:
                raise ValueError(
                    "No paragraph breaks were found (text too short perhaps?)"
                ) from e
        for ts in token_sequences:
            for word, index in ts.wrdindex_list:
                try:
                    while index > current_par_break:
                        current_par_break = next(pb_iter)
                        current_par += 1
                except StopIteration:
                    # hit bottom
                    pass

                if word in token_table:
                    token_table[word].total_count += 1

                    if token_table[word].last_par != current_par:
                        token_table[word].last_par = current_par
                        token_table[word].par_count += 1

                    if token_table[word].last_tok_seq != current_tok_seq:
                        token_table[word].last_tok_seq = current_tok_seq
                        token_table[word].ts_occurences.append([current_tok_seq, 1])
                    else:
                        token_table[word].ts_occurences[-1][1] += 1
                else:  # new word
                    token_table[word] = TokenTableField(
                        first_pos=index,
                        ts_occurences=[[current_tok_seq, 1]],
                        total_count=1,
                        par_count=1,
                        last_par=current_par,
                        last_tok_seq=current_tok_seq,
                    )

            current_tok_seq += 1

        return token_table

    def _depth_scores(self, scores):
        """Calculates the depth of each gap, i.e. the average difference
        between the left and right peaks and the gap's score"""

        depth_scores = [0 for x in scores]
        # clip boundaries: this holds on the rule of thumb(my thumb)
        # that a section shouldn't be smaller than at least 2
        # pseudosentences for small texts and around 5 for larger ones.

        clip = min(max(len(scores) // 10, 2), 5)
        index = clip

        for gapscore in scores[clip:-clip]:
            lpeak = gapscore
            for score in scores[index::-1]:
                if score >= lpeak:
                    lpeak = score
                else:
                    break
            rpeak = gapscore
            for score in scores[index:]:
                if score >= rpeak:
                    rpeak = score
                else:
                    break
            depth_scores[index] = lpeak + rpeak - 2 * gapscore
            index += 1

        return depth_scores

    def _divide_to_tokensequences(self, text):
        "Divides the text into pseudosentences of fixed size"
        w = self.w
        nlp = spacy.load("en_core_web_sm")
        #wrdindex_list = []
            #utterances = [list(re.finditer(r"\w+", u)) for u in text.split('\n\n')]
        utterances = text.split('\n\n')
        split_utterances = [list(re.finditer(r"\w+", u)) for u in utterances]
        # for u in utterances:
        #     print(len(u), [match.group() for match in u])
        #print(utterances)
        token_sequences = []
        split_text = []
        tokenized_text = []
        num_joined_utterances = []

        i, c = 0, 0
        while i < len(utterances):
            current_len = len(split_utterances[i])
            min_diff = abs(current_len - w)
            j = i + 1
            #print(min_diff, abs(current_len + len(utterances[j]) - w))
            while j < len(split_utterances) and min_diff > abs(current_len + len(split_utterances[j]) - w):
                current_len += len(split_utterances[j])
                min_diff = abs(current_len - w)
                j += 1
                # if j < len(utterances):
                #     print(min_diff, abs(current_len + len(utterances[j]) - w))
                # else:
                #     print(min_diff, "END")
            #print(f"---> {i} : {j}")
            token_sequences.append(TokenSequence(c, [(match.group(), match.start()) for u in split_utterances[i:j] for match in u]))
            #tokenized_text.append(sum([nltk.pos_tag(nltk.word_tokenize(u)) for u in utterances[i:j]], []))
            tokenized_text.append([nlp(u) for u in utterances[i:j]])
            split_text.append('\n\n'.join(utterances[i:j]))
            num_joined_utterances.append(j-i)
            i = j
            c += 1

        # matches = re.finditer(r"\w+", text)
        # for match in matches:
        #     wrdindex_list.append((match.group(), match.start()))
        # return [
        #     TokenSequence(int(i / w), wrdindex_list[i : i + w])
        #     for i in range(0, len(wrdindex_list), w)
        # ]
        # for ts in token_sequences[:5]:
        #     print(ts)
        return token_sequences, split_text, tokenized_text, num_joined_utterances
        

    def _identify_boundaries(self, depth_scores, N = None):
        """Identifies boundaries at the peaks of similarity score
        differences"""

        boundaries = [0 for x in depth_scores]

        avg = sum(depth_scores) / len(depth_scores)
        stdev = numpy.std(depth_scores)

        if self.cutoff_policy == LC:
            cutoff = avg - stdev
        else:
            cutoff = avg - stdev / 2.0

        depth_tuples = sorted(zip(depth_scores, range(len(depth_scores))))
        depth_tuples.reverse()
        hp = list(filter(lambda x: x[0] > cutoff, depth_tuples))

        for dt in hp:
            boundaries[dt[1]] = 1
            for dt2 in hp:  # undo if there is a boundary close already
                if (
                    dt[1] != dt2[1]
                    and abs(dt2[1] - dt[1]) < 4
                    and boundaries[dt2[1]] == 1
                ):
                    boundaries[dt[1]] = 0

        if N is not None:
            depth_tuples.sort(key = lambda x: x[1])
            depth_of_boundaries = sorted([depth_tuples[i] for i,b in enumerate(boundaries) if b], key = lambda x: x[0], reverse = True)
            for _,i in depth_of_boundaries[N:]:
                boundaries[i] = 0
        
        return boundaries




    def _mark_paragraph_breaks(self, text):
        """Identifies indented text or line breaks as the beginning of
        paragraphs"""
        MIN_PARAGRAPH = 1#00
        pattern = re.compile("[ \t\r\f\v]*\n[ \t\r\f\v]*\n[ \t\r\f\v]*")
        matches = pattern.finditer(text)

        last_break = 0
        pbreaks = [0]
        for pb in matches:
            if pb.start() - last_break < MIN_PARAGRAPH:
                continue
            else:
                pbreaks.append(pb.start())
                last_break = pb.start()

        return pbreaks

    # def _normalize_boundaries(self, utterances, boundaries, paragraph_breaks):
    #     """Normalize the boundaries identified to the original text's
    #     paragraph breaks"""

    #     norm_boundaries = []
    #     char_count, word_count, gaps_seen = 0, 0, 0
    #     seen_word = False

    #     for char in text:
    #         char_count += 1
    #         if char in " \t\n" and seen_word: # We saw a word and a character that indicates its end (space, new line, etc.)
    #             seen_word = False
    #             word_count += 1
    #         if char not in " \t\n" and not seen_word: # We see a character that belongs to a word
    #             seen_word = True
    #         if gaps_seen < len(boundaries) and word_count > (
    #             max(gaps_seen * self.w, self.w)
    #         ): # we haven't reached the last gap and we saw enough words to reach a new gap
    #             if boundaries[gaps_seen] == 1: # if there is a boundary at this gap
    #                 # find closest paragraph break
    #                 best_fit = len(text) # the largest distance between a 'false' break and a 'real' one is the length of the text
    #                 for br in paragraph_breaks: # br is a 'real' break
    #                     if best_fit > abs(br - char_count): # if we find a break closer than the one we had before
    #                         best_fit = abs(br - char_count) # we record the distance
    #                         bestbr = br # we record the new best break
    #                     else: # if we find a larger distance, we have passed the right break already
    #                         break
    #                 if bestbr not in norm_boundaries:  # avoid duplicates
    #                     norm_boundaries.append(bestbr)
    #             gaps_seen += 1

    #     return norm_boundaries

    def _smooth_scores(self, gap_scores, window='flat'):
        "Wraps the smooth function from the SciPy Cookbook"
        return list(
            smooth(numpy.array(gap_scores[:]), window_len=self.smoothing_width + 1, window=window)
        )


    def _stem_words(self, wrdindex_list, stemmer):
        return [(stemmer.stem(w), i) for w, i in wrdindex_list]

    def _lemmatize_words(self, wrdindex_list, lemmatizer):
        res = [(lemmatizer.lemmatize(w), i) for w, i in wrdindex_list]
        return res

    def _vocabulary_introduction(self, tokseqs, token_table, memory_size = None):
        """Implements the vocabulary introduction method"""

        def vi_frq(block, memory_size):
            freq = 0
            for ts in block:
                for tok, i in ts.wrdindex_list:
                    seen_words_in_ts = set()
                    if token_table[tok].first_pos == i: # never seen before
                        freq += 1
                    elif memory_size is not None and tok not in seen_words_in_ts:
                        tok_exists_in_ts = {occ[0] for occ in token_table[tok].ts_occurences}
                        if not tok_exists_in_ts.intersection(set(range(ts.ts_index - memory_size, ts.ts_index))):
                            freq += 1
                    seen_words_in_ts.add(tok)
            return freq

        gap_scores = []
        numgaps = len(tokseqs) - 1

        for curr_gap in range(numgaps):
            # adjust window size for boundary conditions (i.e. if k is too small for left or right, make the window smaller on both sides)
            if curr_gap < self.k - 1:
                window_size = curr_gap + 1
            elif curr_gap > numgaps - self.k:
                window_size = numgaps - curr_gap
            else:
                window_size = self.k

            b1 = tokseqs[curr_gap - window_size + 1 : curr_gap + 1]
            b2 = tokseqs[curr_gap + 1 : curr_gap + window_size + 1]

            len_b1 = sum([ts.original_length for ts in b1])
            len_b2 = sum([ts.original_length for ts in b2])

            score = (vi_frq(b1, memory_size) + vi_frq(b2, memory_size))/(len_b1 + len_b2)

            gap_scores.append(score)

        return gap_scores



class TokenTableField:
    """A field in the token table holding parameters for each token,
    used later in the process"""

    def __init__(
        self,
        first_pos,
        ts_occurences,
        total_count=1,
        par_count=1,
        last_par=0,
        last_tok_seq=None,
    ):
        self.__dict__.update(locals())
        del self.__dict__["self"]

    def __repr__(self):
        return f"first_pos = {self.first_pos}, ts_occurences = {self.ts_occurences}, total_count = {self.total_count}, par_count = {self.par_count}, last_par = {self.last_par}, last_tok_seq = {self.last_tok_seq}"

    def __str__(self):
        return f"first_pos = {self.first_pos}, ts_occurences = {self.ts_occurences}, total_count = {self.total_count}, par_count = {self.par_count}, last_par = {self.last_par}, last_tok_seq = {self.last_tok_seq}"



class TokenSequence:
    "A token list with its original length and its index"

    def __init__(self, ts_index, wrdindex_list, original_length=None):
        original_length = original_length or len(wrdindex_list)
        self.__dict__.update(locals())
        del self.__dict__["self"]

    def __repr__(self):
        return f"{self.ts_index} ({self.original_length}): {str(self.wrdindex_list)}"

    def __str__(self):
        return f"{self.ts_index} ({self.original_length}): {str(self.wrdindex_list)}"



# Pasted from the SciPy cookbook: https://www.scipy.org/Cookbook/SignalSmooth
def smooth(x, window_len=11, window="flat"):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the beginning and end part of the output signal.

    :param x: the input signal
    :param window_len: the dimension of the smoothing window; should be an odd integer
    :param window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
        flat window will produce a moving average smoothing.

    :return: the smoothed signal

    example::

        t=linspace(-2,2,0.1)
        x=sin(t)+randn(len(t))*0.1
        y=smooth(x)

    :see also: numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve,
        scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    if window not in ["flat", "hanning", "hamming", "bartlett", "blackman"]:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
        )

    s = numpy.r_[2 * x[0] - x[window_len:1:-1], x, 2 * x[-1] - x[-1:-window_len:-1]]

    # print(len(s))
    if window == "flat":  # moving average
        w = numpy.ones(window_len, "d")
    else:
        w = eval("numpy." + window + "(window_len)")

    y = numpy.convolve(w / w.sum(), s, mode="same")

    return y[window_len - 1 : -window_len + 1]


if __name__ == '__main__':
    import readFriends
    import itertools

    parser = argparse.ArgumentParser(description="Script to segment a text in topics.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-x", "--experience-name", type=str, default='BlockComp', help="name of the experiment")
    parser.add_argument("-s", "--season", nargs='+', type=int, default=None, help="season we train on")
    parser.add_argument("-m", "--method", type=int, default=0, help="the similarity method we use")
    parser.add_argument("-p", "--participants", type=int, default=1, help="the speaker method we use")
    parser.add_argument("-q", "--questions", type=bool, default=False, help="if we use the questions or not")
    parser.add_argument("-l", "--stem", type=bool, default=False, help="if we stem or not")
    parser.add_argument("-t", "--lemm", type=bool, default=False, help="if we lemmatise or not")
    parser.add_argument("-i", "--iteration", type=str, default='', help="the iteration of the experiment for saving purposes")
    args = parser.parse_args()
    config = vars(args)
    print(config)

    xp_name = config["experience_name"] # "secn" # n
    seasons = config["season"]
    iteration = config["iteration"]

    device = "cuda" if torch.cuda.is_available() else "cpu"
    #device = "mps" if torch.backends.mps.is_available() else "cpu"
    print(f"The device is: {device}.\n")
    device = torch.device(device)

    dataset = readFriends.FriendsDataset()

    params = {
        'w': 12,
        'k': 20,
        'memory_size': 20,
        'similarity_method': config["method"],
        'stopwords': None,
        'smoothing_method': DEFAULT_SMOOTHING,
        'smoothing_window': FLAT_WINDOW,
        'smoothing_width': 2,
        'smoothing_rounds': 1,
        'cutoff_policy': HC,
        'stemmer': config["stem"],
        'lemmatizer': config["lemm"],
        'speaker_method': config["participants"],
        'questions': config["questions"],
    }

    all_f1 = []
    all_fk1 = []
    all_fk2 = []
    all_pk = []
    all_ep = []

    for season in seasons:
        results = []
        num_ep = dataset.dataset_stats['Episodes'].loc[str(season)]
        for ep in range(1, num_ep + 1):
            all_ep.append(f's{season}e{ep}')
            df = dataset.as_df(dataset.get_episode(season, ep), speakers = False)
            # Gold seg
            segmentation_gold = []
            for _, l in itertools.groupby(df['topics_scenes_and_notes']):
                segmentation_gold += [0]*(sum(1 for _ in l)-1) + [1]
            
            # Get XP topics
            tt = TextTilingTokenizer(**params)
            _, b = tt.tokenize('\n\n'.join(df['utterance']), df['speakers'])

            pr = precision_score(segmentation_gold[:-1], b)
            rec = recall_score(segmentation_gold[:-1], b)
            f1 = f1_score(segmentation_gold[:-1], b)
            prk1, reck1, fk1 = utils.Fk(segmentation_gold[:-1], b, k=1)
            prk2, reck2, fk2 = utils.Fk(segmentation_gold[:-1], b, k=2)
            pk = seg_mod.pk(segmentation_gold[:-1], b, boundary=1)

            all_f1.append(f1)
            all_fk1.append(fk1)
            all_fk2.append(fk2)
            all_pk.append(pk)

            print(f"Season {season}, Episode {ep}")
        
        print(f"Season {season} OK\n")
    

    # Save results for this XP
    df = pd.DataFrame([all_f1, all_fk1, all_fk2, all_pk], index=['F1', 'Fk1', 'Fk2', 'Pk'], columns=all_ep)
    df.to_csv(f"../results/{xp_name}.csv")
    