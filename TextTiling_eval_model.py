# Small adaptations from the code of Linzi Xing and Giuseppe Carenini:
# Improving Unsupervised Dialogue Topic Segmentation with Utterance-Pair Coherence Scoring
# https://github.com/lxing532/Dialogue-Topic-Segmenter
# https://aclanthology.org/2021.sigdial-1.18.pdf
#
# Modifications by: Authors of the paper

import numpy as np
import itertools
from transformers import BertTokenizer
import torch
from transformers import BertForNextSentencePrediction
import statistics
from sklearn.metrics import f1_score, precision_score, recall_score
from nltk.metrics import segmentation as seg_mod
import pandas as pd
import readFriends
import utils
import argparse

def tokenize(text, tokenizer, model, device, batch_size = 1, demo_mode = False):
    text = list(text)
    gap_scores = carenini_gap_scores_(text, tokenizer, model, device, batch_size)
    smooth_scores = smooth_scores_(gap_scores)
    depth_scores = depth_score_cal_(smooth_scores)
    segment_boundaries = identify_boundaries_(depth_scores)
    # End of Boundary Identification

    if demo_mode:
        return gap_scores, smooth_scores, depth_scores, segment_boundaries

    utterances = []
    segmented_text = []
    topic = [utterances[0]]
    for i, b in enumerate(segment_boundaries):
        if b:
            segmented_text.append('\n\n'.join(topic))
            topic = [utterances[i+1]]
        else:
            topic.append(utterances[i+1])

    if segment_boundaries[-1]: # if 1, last utterance alone as topic
        segmented_text.append(utterances[-1])
    else: # if 0, last topic not included yet
        segmented_text.append('\n\n'.join(topic))
    # if prevb < text_length:  # append any text that may be remaining
    #     segmented_text.append(text[prevb:])

    if not segmented_text: # if no segment yet, return the whole text, ie. 1 single topic
        segmented_text = [text]

    return segmented_text

def depth_score_cal_(scores): # theur implementation of depth score
	output_scores = []
	for i in range(len(scores)):
		lflag = scores[i]; rflag = scores[i];
		if i == 0:
			hl = scores[i]
			for r in range(i+1,len(scores)):
				if rflag <= scores[r]:
					rflag = scores[r]
				else:
					break
		elif i == len(scores):
			hr = scores[i]
			for l in range(i-1, -1, -1):
				if lflag <= scores[l]:
					lflag = scores[l]
				else:
					break
		else:
			for r in range(i+1,len(scores)):
				if rflag <= scores[r]:
					rflag = scores[r]
				else:
					break
			for l in range(i-1, -1, -1):
				if lflag <= scores[l]:
					lflag = scores[l]
				else:
					break
		depth_score = 0.5*(lflag+rflag-2*scores[i])
		output_scores.append(depth_score)

	return output_scores

def smooth_scores_(gap_scores, window='flat'):
    "Wraps the smooth function from the SciPy Cookbook"
    return list(
        smooth(np.array(gap_scores[:]), window_len=3, window=window)
    )

def carenini_gap_scores_(text, tokenizer, model, device, batch_size = 1):
    id_inputs = []
    for i in range(len(text)-1):
        sent1 = text[i]
        sent2 = text[i+1]
        encoded_sent1 = tokenizer.encode(sent1, add_special_tokens = True, truncation = True, max_length = 128, return_tensors = 'pt')
        encoded_sent2 = tokenizer.encode(sent2, add_special_tokens = True, truncation = True, max_length = 128, return_tensors = 'pt')
        encoded_pair = encoded_sent1[0].tolist() + encoded_sent2[0].tolist()[1:]
        id_inputs.append(torch.Tensor(encoded_pair))
	
    MAX_LEN = 256
    id_inputs = torch.nn.utils.rnn.pad_sequence(id_inputs + [torch.zeros(MAX_LEN)], batch_first = True)[:-1].tolist()
    attention_masks = []
    for sent in id_inputs:
        att_mask = [int(token_id > 0) for token_id in sent]
        attention_masks.append(att_mask)

    test_inputs = torch.tensor(id_inputs).to(device).long()
    test_masks = torch.tensor(attention_masks).to(device).int()

    all_scores = []
    for i in range(0, test_inputs.shape[0], batch_size):
        scores = model(test_inputs[i:i+batch_size], attention_mask=test_masks[i:i+batch_size])
        all_scores += torch.sigmoid(scores[0][:,0]).detach().cpu().numpy().tolist()

    return all_scores

def identify_boundaries_(depth_scores):
    threshold = sum(depth_scores)/(len(depth_scores))-0.1*statistics.stdev(depth_scores)
    boundary_indice = []

    seg_p_labels = [0]*(len(depth_scores))
    
    for i in range(len(depth_scores)):
        if depth_scores[i] > threshold:
            boundary_indice.append(i)
	
    for i in boundary_indice:
        seg_p_labels[i] = 1

    tmp = 0; seg_p = []
    for fake in seg_p_labels:
        if fake == 1:
            tmp += 1
            seg_p.append(tmp)
            tmp = 0
        else:
            tmp += 1
    seg_p.append(tmp+1)

    return seg_p_labels

# Pasted from the SciPy cookbook: https://www.scipy.org/Cookbook/SignalSmooth
def smooth(x, window_len=11, window="flat"):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the beginning and end part of the output signal.

    :param x: the input signal
    :param window_len: the dimension of the smoothing window; should be an odd integer
    :param window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
        flat window will produce a moving average smoothing.

    :return: the smoothed signal

    example::

        t=linspace(-2,2,0.1)
        x=sin(t)+randn(len(t))*0.1
        y=smooth(x)

    :see also: numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve,
        scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    if window not in ["flat", "hanning", "hamming", "bartlett", "blackman"]:
        raise ValueError(
            "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
        )

    s = np.r_[2 * x[0] - x[window_len:1:-1], x, 2 * x[-1] - x[-1:-window_len:-1]]

    # print(len(s))
    if window == "flat":  # moving average
        w = np.ones(window_len, "d")
    else:
        w = eval("numpy." + window + "(window_len)")

    y = np.convolve(w / w.sum(), s, mode="same")

    return y[window_len - 1 : -window_len + 1]


parser = argparse.ArgumentParser(description="Script to train the similarity score model.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-x", "--experience-name", type=str, default='secn', help="name of the experiment")
parser.add_argument("-e", "--epochs", nargs='+', type=int, default=list(range(0, 10)), help="the epoch files to evaluate")
parser.add_argument("-b", "--batch-size", type=int, default=1, help="size of the data batches")
parser.add_argument("-s", "--season", nargs='+', type=int, default=[], help="season we evaluate on")
parser.add_argument("-k", "--kvalue", type=int, default=2, help="the k for the Fk measure")
parser.add_argument("-i", "--iteration", type=str, default='', help="the iteration of the experiment for saving purposes")
args = parser.parse_args()
config = vars(args)
print(config)

xp_name = config["experience_name"] # "secn" # n
epochs = config["epochs"] # 10
batch_size = config["batch_size"] # 32
seasons = config["season"]
iteration = config["iteration"]
kvalue = config["kvalue"]

device = "cuda" if torch.cuda.is_available() else "cpu"
#device = "mps" if torch.backends.mps.is_available() else "cpu"
print(f"The device is: {device}.\n")
device = torch.device(device)
#device = 0

if not seasons:
    seasons = []
    for c in xp_name:
        if c.isnumeric():
            seasons.append(int(c))

for epoch_file in epochs:
    MODEL_PATH = f'./trained_bert/model_{xp_name}{iteration}/m{epoch_file}'
    #MODEL_PATH = f'./OG/model/modelOG/m{epoch_file}'
    #MODEL_PATH = 'bert-base-uncased'
    model = BertForNextSentencePrediction.from_pretrained(MODEL_PATH, num_labels = 2, output_attentions = False, output_hidden_states = False)
    #model.cuda(device)
    model.to(device)
    '''
    MODEL_PATH = '/scratch/linzi/bert_9'
    model.load_state_dict(torch.load(MODEL_PATH ,map_location=device))
    '''
    model.eval()

    dataset = readFriends.FriendsDataset()
    
    print(f"Epoch {epoch_file}.")
    print('\tLoading BERT tokenizer...')
    TOKENIZER_PATH = f'./trained_bert/tokenizer_{xp_name}{iteration}/t{epoch_file}'
    #TOKENIZER_PATH = f'./OG/model/tokenizerOG/t{epoch_file}'
    tokenizer = BertTokenizer.from_pretrained(TOKENIZER_PATH, do_lower_case=True)

    all_f1 = []
    all_fk1 = []
    all_fk2 = []
    all_pk = []
    all_ep = []


    print("\t---------- Start evaluation ----------")
    for season in seasons:
        results = []
        print(f"\tSeason {season}")
        num_ep = dataset.dataset_stats['Episodes'].loc[str(season)]
        for ep in range(1, num_ep + 1):
            all_ep.append(f's{season}e{ep}')
            df = dataset.as_df(dataset.get_episode(season, ep))
            # Gold seg
            segmentation_gold = []
            for _, l in itertools.groupby(df['topics_scenes_and_notes']):
                segmentation_gold += [0]*(sum(1 for _ in l)-1) + [1]

            gap_scores, smooth_scores, depth_scores, segmentation_hyp = tokenize(df['utterance'], 
                                                                                   tokenizer, model, device, 
                                                                                   demo_mode = True,
                                                                                   batch_size = batch_size)

            pr = precision_score(segmentation_gold[:-1], segmentation_hyp)
            rec = recall_score(segmentation_gold[:-1], segmentation_hyp)
            f1 = f1_score(segmentation_gold[:-1], segmentation_hyp)
            prk1, reck1, fk1 = utils.Fk(segmentation_gold[:-1], segmentation_hyp, k=1)
            prk2, reck2, fk2 = utils.Fk(segmentation_gold[:-1], segmentation_hyp, k=2)
            pk = seg_mod.pk(segmentation_gold[:-1], segmentation_hyp, boundary=1)

            all_f1.append(f1)
            all_fk1.append(fk1)
            all_fk2.append(fk2)
            all_pk.append(pk)

            print(f"\t\tEpisode {ep}: {f1} -- {fk1} -- {fk2} -- {pk}")
            

    # Save results for this XP
    df = pd.DataFrame([all_f1, all_fk1, all_fk2, all_pk], index=['F1', 'Fk1', 'Fk2', 'Pk'], columns=all_ep)
    df.to_csv(f"../results/{xp_name}_e{epoch_file}.csv")