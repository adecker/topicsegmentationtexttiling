import random
import argparse
import re

def load_data(in_fname, seasons = list(range(2, 11))):
    id2txt = {}
    id2info = {}
    with open(in_fname) as in_file:
        for idx, line in enumerate(in_file):
            data = line.split('\t')
            if int(data[0]) in seasons:
                id2txt[idx] = [utterance.replace(" __eou__","") for utterance in data[-1].strip().split(" __eou__ ")]
                id2info[idx] = data[:-1]#{'s': int(data[0]), 'e': int(data[1]), 'c': int(data[2]), 'n': int(data[3])}
    return id2txt, id2info

def in_valid_texts(base_infos, test_infos, diff_id):
    if base_infos[diff_id] == test_infos[diff_id]:
        return False
    else:
        for i in range(diff_id):
            if base_infos[i] != test_infos[i]:
                return False
    return True


parser = argparse.ArgumentParser(description="Script to train the similarity score model.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-p", "--wanted-pairs", type=str, default='e', help="The pairs to test among d,s,e, and c: 'd' for different season, 's' for diff episode / same season, 'e' for diff scene / same episdoe and 'c' for different note / same scene.")
parser.add_argument("-s", "--seasons", nargs='+', type=int, default=list(range(2, 11)), help="the seasons to use in training data")
parser.add_argument("-n", "--experience-name", type=str, default='ML_c', help="Something to add at the end of the file names if needed.")
args = parser.parse_args()
config = vars(args)

possible_pairs = {'d': 0, 's': 1, 'e': 2, 'c': 3} 
# 'd' for different season, 's' for diff episode in the same season, 
# 'e' for diff scene in the same episode and 'c' for different notes in the same scene
wanted_pairs = {k: v for k,v in possible_pairs.items() if k in config["wanted_pairs"]}
seasons = config["seasons"]
xp_name = config["experience_name"]

data_path = './dialogues.txt'
# load all the dialogues and their features...
txt_dict, info_dict = load_data(data_path, seasons)

# extract the utterance pairs
tuples = []; win_size = 1; count_no_n = 0; count_no_other = [0,0,0,0]; count_ex = 0
for txt_id, utterances in txt_dict.items():

    infos = info_dict[txt_id]

    for u_id, u in enumerate(utterances[:-1]):
        count_ex += 1
        positive_sample = [u, utterances[u_id+1]] # subsequent utterances (coherence layer [a] in the paper)
        # Negative sample in same same note (coherence layer [n] in the paper)
        used_ids = [txt_id]
        try:
            if u_id-1-win_size < 0:
                negative_sample_n = [u,random.choice(utterances[u_id+1+win_size:])]
            else:
                negative_sample_n = [u, random.choice(utterances[:u_id-win_size]+utterances[u_id+1+win_size:])]
        except:
            #print('there is no negative sample n...')
            count_no_n += 1
            negative_sample_n = []

        # This is the part where the more different layers should be added
        negative_samples = []
        for pair_type, first_diff_id in wanted_pairs.items():
            try:
                txt_key, txt_infos = random.choice([(key, value) for key, value in info_dict.items() if key not in used_ids and in_valid_texts(infos, value, first_diff_id)])
                sampled_dial = txt_dict[txt_key]
                used_ids.append(txt_key)
                negative_samples.append([u, random.choice(sampled_dial)])
            except:
                count_no_other[first_diff_id] += 1
    
        if negative_sample_n:
            tmp = [positive_sample, negative_sample_n] + negative_samples
            tuples.append(tmp)
        else:
            tmp = [positive_sample] + negative_samples
            tuples.append(tmp)
print(len(tuples))
print(count_no_n, count_no_other, count_ex)

sample_num_memory = []

with  open(f'./training_data/friends_pairs_{"".join(wanted_pairs.keys())}{xp_name}.txt',"w+") as f:
    for tup in tuples:
        sample_num_memory.append(len(tup))
        for pir in tup:
            sent1 = re.sub(r'\s([,?.!"](?:\s|$))', r'\1', pir[0])
            sent2 = re.sub(r'\s([,?.!"](?:\s|$))', r'\1', pir[1])
            f.write(sent1+'\t\t'+sent2)
            f.write('\n')
print("Pairs saved.")

with open(f'./training_data/friends_sample_num_{"".join(wanted_pairs.keys())}{xp_name}.txt',"w+") as f:
    for sample_size in sample_num_memory:
        f.write(str(sample_size))
        f.write('\n')
print("Sample nums saved.")
