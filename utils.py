import pandas as pd
import re
import itertools
import numpy as np

def Fk(gold, hyp, k=2, beta=2):
    valid_hyp = 0
    temp_gold = gold[:]
    for i, b in enumerate(hyp):
        if b:
            l_inf = max(0, i-k)
            l_sup = min(i+k+1, len(hyp))
            try:
                closest_valid_i = temp_gold.index(1, l_inf, l_sup)
            except ValueError:
                pass
            else:
                valid_hyp += 1
                temp_gold[closest_valid_i] = 0
    if sum(hyp):
        pr = valid_hyp/sum(hyp)
    else:
        pr = 0
    re = valid_hyp/sum(gold)
    if beta**2*pr+re:
        return pr, re, (1+beta**2)*pr*re/(beta**2*pr+re)
    else:
        return pr, re, 0

